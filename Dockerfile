FROM ubuntu:18.04

RUN apt-get update 

RUN apt-get install -y libboost-all-dev texinfo nano wget bison flex build-essential sudo 

RUN apt-get autoclean
COPY tools tools
RUN ./tools/install-sdcc.sh 
RUN ./tools/install-stm8-binutils.sh

