#!/bin/sh
set -ex
cd /tmp/
wget https://downloads.sourceforge.net/sourceforge/sdcc/sdcc-src-4.1.0.tar.bz2
tar -xf sdcc-src-4.1.0.tar.bz2
cd sdcc
./configure --disable-pic14-port --disable-pic16-port
make
sudo make install
